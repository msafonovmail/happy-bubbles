//
// Created by vlad on 03.12.16.
//

#include "Bubble.h"

Bubble *Bubble::create() {
    Bubble *bubble = new Bubble();

    if (bubble->initWithFile("bubble.png")) {
        bubble->autorelease();
        return bubble;
    } else {
        CC_SAFE_DELETE(bubble);
        return nullptr;
    }
}
