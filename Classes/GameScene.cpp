#include <iostream>
#include "GameScene.h"
#include "Logger.h"

USING_NS_CC;


Scene *GameScene::createScene() {
    auto scene = Scene::create();
    auto layer = GameScene::create();
    scene->addChild(layer);
    return scene;
}


bool GameScene::init() {
    if (!Layer::init()) {
        return false;
    }

    auto size = Director::getInstance()->getVisibleSize();
    auto origin = Director::getInstance()->getVisibleOrigin();

    Logger::log() << " width = " << size.width
                  << " height = " << size.height << "\n";

    Logger::log() << " origin.x = " << origin.x
                  << " origin.y = " << origin.y << "\n";

    Logger::log() << " layer.x = " << getPosition().x
                  << " layer.y = " << getPosition().y << "\n";

    srand((unsigned int) time(0));

    auto listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = [this](Touch *t, Event *e) {
        if (e->isStopped()) {
            return false;
        }
        auto bubble = static_cast<Bubble *>(e->getCurrentTarget());
        auto loc = t->getLocation();
        auto convertedLoc = bubble->convertToNodeSpace(loc) + bubble->getContentSize() / 2;
        auto len = (convertedLoc + bubble->getContentSize() / 2).getLength();
        Logger::log() << " loc = {" << loc.x << ", " << loc.y << "}"
                      << " converted = {" << convertedLoc.x << ", " << convertedLoc.y << "}"
                      << " len = " << len << "\n";
        Vec2 touchPos = bubble->convertToNodeSpace(t->getLocation()) -
                        bubble->getContentSize() / 2;
        if (touchPos.getLengthSq() < 75 * 75) {
            bubble->removeFromParent();
            auto iter = std::find(mBubbles.begin(), mBubbles.end(), bubble);
            if (iter != mBubbles.end()) {
                mBubbles.erase(iter);
            }
            e->stopPropagation();
            return true;
        } else {
            return false;
        }
    };

    for (int i = 0; i < 10; i++) {
        auto newBubble = Bubble::create();
        auto newBubbleSize = newBubble->getContentSize();

        Logger::log() << " width = " << newBubbleSize.width
                      << " height = " << newBubbleSize.height << "\n";

        Vec2 newBubblePos;
        newBubblePos.x = origin.x + newBubbleSize.width / 2 +
                         ((float) rand() / RAND_MAX) * (size.width - newBubbleSize.width);
        newBubblePos.y = origin.y + newBubbleSize.height / 2 +
                         ((float) rand() / RAND_MAX) * (size.height - newBubbleSize.height);
        Logger::log() << " genX = " << newBubblePos.x
                      << " genY = " << newBubblePos.y << "\n";

        Vec2 newBubbleSpeed;
        newBubbleSpeed.x = rand() - RAND_MAX / 2;
        newBubbleSpeed.y = rand() - RAND_MAX / 2;
        newBubbleSpeed.normalize();
        newBubbleSpeed *= 100;

        newBubble->setAnchorPoint(Vec2(0.5, 0.5));
        newBubble->setPosition(newBubblePos);
        newBubble->setSpeed(newBubbleSpeed);

        _eventDispatcher->addEventListenerWithSceneGraphPriority(listener->clone(), newBubble);
        addChild(newBubble);
        mBubbles.push_back(newBubble);
    }

    scheduleUpdate();
    return true;
}

void GameScene::update(float dt) {
    auto screenSize = Director::getInstance()->getVisibleSize();
    auto screenOrigin = Director::getInstance()->getVisibleOrigin();

    for (auto &bubble : mBubbles) {
        auto speed = bubble->getSpeed();
        auto pos = bubble->getPosition();
        auto size = bubble->getContentSize();
        if (pos.x >= screenOrigin.x + screenSize.width - size.width / 2 && speed.x > 0) {
            speed.x = -speed.x;
        } else if (pos.x <= screenOrigin.x + size.width / 2 && speed.x < 0) {
            speed.x = -speed.x;
        }
        if (pos.y >= screenOrigin.y + screenSize.height - size.height / 2 && speed.y > 0) {
            speed.y = -speed.y;
        } else if (pos.y <= screenOrigin.y + size.height / 2 && speed.y < 0) {
            speed.y = -speed.y;
        }
        bubble->setSpeed(speed);
    }

    for (auto &bubble : mBubbles) {
        bubble->setPosition(bubble->getPosition() + bubble->getSpeed() * dt);
    }
}
