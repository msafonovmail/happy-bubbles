//
// Created by vlad on 03.12.16.
//

#ifndef MYGAME_BUBBLE_H
#define MYGAME_BUBBLE_H


#include <math/Vec2.h>
#include <2d/CCSprite.h>

class Bubble : public cocos2d::Sprite {
public:
    static Bubble* create();

    inline const cocos2d::Vec2 &getSpeed() const {
        return mSpeed;
    }

    inline void setSpeed(const cocos2d::Vec2 &speed) {
        Bubble::mSpeed = speed;
    }

private:
    cocos2d::Vec2 mSpeed;
};


#endif //MYGAME_BUBBLE_H
