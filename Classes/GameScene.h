#ifndef __GAME_SCENE_H__
#define __GAME_SCENE_H__

#include "cocos2d.h"
#include "Bubble.h"
#include <vector>

class GameScene : public cocos2d::Layer {
public:
    CREATE_FUNC(GameScene);

    static cocos2d::Scene *createScene();

    virtual bool init();

    virtual void update(float dt) override;

private:
    std::vector<Bubble *> mBubbles;
};

#endif // __GAME_SCENE_H__
