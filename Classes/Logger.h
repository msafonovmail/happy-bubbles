//
// Created by vlad on 03.12.16.
//

#ifndef MYGAME_LOGGER_H
#define MYGAME_LOGGER_H


#include <string>
#include <iostream>

class Logger {
public:
    inline static Logger &log() {
        return l;
    }

    template<typename T>
    inline Logger &operator<<(T &a) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32) || (CC_TARGET_PLATFORM == CC_PLATFORM_MAC) || (CC_TARGET_PLATFORM == CC_PLATFORM_LINUX)
        std::cout << a;
#endif
        return *this;
    }

private:
    static Logger l;
};

#endif //MYGAME_LOGGER_H
